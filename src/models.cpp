#include "models.h"

#include <QBrush>

CrossedModel::CrossedModel(QObject *parent) : QAbstractTableModel(parent) {
    objects.clear();
}

int CrossedModel::rowCount(const QModelIndex &parent) const {
    Q_UNUSED(parent);
    return objects.size();
}
int CrossedModel::columnCount(const QModelIndex &parent) const {
    Q_UNUSED(parent);
    return 23;
}
QVariant CrossedModel::data(const QModelIndex &index, int role) const {
    if (!index.isValid()) {
        return QVariant();
    }
    if (index.row() >= objects.size()) {
        return QVariant();
    }
    int i = index.row();
    int j = index.column();
    if (role == Qt::DisplayRole) {
        switch(j){
            case 0: return QVariant(objects[i].accountNumber);
            case 1: return QVariant(objects[i].rutA);
            case 2: return QVariant(objects[i].enterpriseCode);
            case 3: return QVariant(objects[i].productCode);
            case 4: return QVariant(objects[i].datePost);
            case 5: return QVariant((quint32)objects[i].amountPost);
            case 6: return QVariant(objects[i].dateContract.toString("yyyy-MM-dd"));

            case 7: return QVariant(objects[i].rutFather);
            case 8: return QVariant(objects[i].fFirstName);
            case 9: return QVariant(objects[i].fLastName);
            case 10: return QVariant(objects[i].rutMother);
            case 11: return QVariant(objects[i].mFirstName);
            case 12: return QVariant(objects[i].mLastName);
            case 13: return QVariant(objects[i].marriageDate.toString("yyyy-MM-dd"));
            case 14: return QVariant(objects[i].childrenNumber);
            case 15: return QVariant(objects[i].childrenNumberUnderAge);
            case 16: return QVariant(objects[i].address);

            case 17: return QVariant(objects[i].rutB);
            case 18: return QVariant(objects[i].firstName);
            case 19: return QVariant(objects[i].lastName);
            case 20: return QVariant((quint32)objects[i].actives);
            case 21: return QVariant(objects[i].code);
            case 22: return QVariant(objects[i].gloss);
        }
    }
    return QVariant();
}
QVariant CrossedModel::headerData(int section, Qt::Orientation orientation, int role) const {
    if (role != Qt::DisplayRole) {
        return QVariant();
    }
    if (orientation == Qt::Horizontal) {
        switch(section) {
        case 0: return "Numero de Cuenta";
        case 1: return "Rut Asociado";
        case 2: return "Codigo Empresa";
        case 3: return "Codigo Producto";
        case 4: return "Fecha Posteo";
        case 5: return "Monto Posteo";
        case 6: return "Fecha Contrato";

        case 7: return "Rut Padre";
        case 8: return "Nombres";
        case 9: return "Apellidos";
        case 10: return "Rut Madre";
        case 11: return "Nombres";
        case 12: return "Apellidos";
        case 13: return "Fecha Contrato Matrimonio";
        case 14: return "Numero de Hijos";
        case 15: return "Numero de Hijos menores de 18";
        case 16: return "Direccion";

        case 17: return "RUT";
        case 18: return "Nombre";
        case 19: return "Apellidos";
        case 20: return "Activos o Patrimonios";
        case 21: return "Codigo";
        case 22: return "Glosa";
        }
    }
    return QVariant();
}
Qt::ItemFlags CrossedModel::flags(const QModelIndex &index) const {
    if (!index.isValid()) {
        return Qt::ItemIsEnabled;
    }
    return QAbstractTableModel::flags(index);
}

void CrossedModel::setList(const QList<CrossedEntity> &list) {
    beginResetModel();
    objects = list;
    endResetModel();
}

//--------------------------------------------------------------------------------------------------

PendingModel::PendingModel(QObject *parent) : QAbstractTableModel(parent), mListener(NULL) {
    objects.clear();
}

int PendingModel::rowCount(const QModelIndex &parent) const {
    Q_UNUSED(parent);
    return objects.size();
}
int PendingModel::columnCount(const QModelIndex &parent) const {
    Q_UNUSED(parent);
    return 23;
}
QVariant PendingModel::data(const QModelIndex &index, int role) const {
    if (!index.isValid()) {
        return QVariant();
    }
    if (index.row() >= objects.size()) {
        return QVariant();
    }
    int i = index.row();
    int j = index.column();
    const PendingEntity &o = objects[i];

    if (role == Qt::DisplayRole || role == Qt::EditRole) {

        switch(j){
            case 0: return QVariant(o.accountNumber);
            case 1: return QVariant(o.rutA);
            case 2: return QVariant(o.enterpriseCode);
            case 3: return QVariant(o.productCode);
            case 4: return QVariant(o.datePost);
            case 5: return QVariant(o.amountPost);
            case 6: return QVariant(o.dateContract);

            case 7: return QVariant(o.rutFather);
            case 8: return QVariant(o.fFirstName);
            case 9: return QVariant(o.fLastName);
            case 10: return QVariant(o.rutMother);
            case 11: return QVariant(o.mFirstName);
            case 12: return QVariant(o.mLastName);
            case 13: return QVariant(o.marriageDate);
            case 14: return QVariant(o.childrenNumber);
            case 15: return QVariant(o.childrenNumberUnderAge);
            case 16: return QVariant(o.address);

            case 17: return QVariant(o.rutB);
            case 18: return QVariant(o.firstName);
            case 19: return QVariant(o.lastName);
            case 20: return QVariant(o.actives);
            case 21: return QVariant(o.code);
            case 22: return QVariant(o.gloss);
        }
    } else if (role == Qt::BackgroundRole) {
        static QVariant COLOR_OK(QBrush(QColor(200, 255, 200)));
        static QVariant COLOR_BAD(QBrush(QColor(255, 200, 200)));
        static QVariant COLOR_IGNORED(QBrush(QColor(255, 255, 200)));

        if (o.isIgnored()) {
            return COLOR_IGNORED;
        }

        static QString s;
        switch(j){
            case 0: return mValidator.validUnsignedInteger(o.accountNumber, s) ? COLOR_OK : COLOR_BAD;
            case 1: return mValidator.validRut(o.rutA, s) ? COLOR_OK : COLOR_BAD;
            case 2: return mValidator.validEnterpriseCode(o.enterpriseCode, s) ? COLOR_OK : COLOR_BAD;
            case 3: return mValidator.validProductCode(o.productCode, s) ? COLOR_OK : COLOR_BAD;
            case 4: return mValidator.validDate(o.datePost, s) ? COLOR_OK : COLOR_BAD;
            case 5: return mValidator.validMoneyAmount(o.amountPost, s) ? COLOR_OK : COLOR_BAD;
            case 6: return mValidator.validDate(o.dateContract, s) ? COLOR_OK : COLOR_BAD;

            case 7: return mValidator.validRut(o.rutFather, s) ? COLOR_OK : COLOR_BAD;
            case 8: return mValidator.validName(o.fFirstName, s) ? COLOR_OK : COLOR_BAD;
            case 9: return mValidator.validName(o.fLastName, s) ? COLOR_OK : COLOR_BAD;
            case 10: return mValidator.validRut(o.rutMother, s) ? COLOR_OK : COLOR_BAD;
            case 11: return mValidator.validName(o.mFirstName, s) ? COLOR_OK : COLOR_BAD;
            case 12: return mValidator.validName(o.mLastName, s) ? COLOR_OK : COLOR_BAD;
            case 13: return mValidator.validDate(o.marriageDate, s) ? COLOR_OK : COLOR_BAD;
            case 14: return mValidator.validUnsignedInteger(o.childrenNumber, s) ? COLOR_OK : COLOR_BAD;
            case 15: return mValidator.validUnsignedInteger(o.childrenNumberUnderAge, s) ? COLOR_OK : COLOR_BAD;
            case 16: return mValidator.validAddress(o.address, s) ? COLOR_OK : COLOR_BAD;

            case 17: return mValidator.validRut(o.rutB, s) ? COLOR_OK : COLOR_BAD;
            case 18: return mValidator.validName(o.firstName, s) ? COLOR_OK : COLOR_BAD;
            case 19: return mValidator.validName(o.lastName, s) ? COLOR_OK : COLOR_BAD;
            case 20: return mValidator.validMoneyAmount(o.actives, s) ? COLOR_OK : COLOR_BAD;
            case 21: return mValidator.validSiiCode(o.code, s) ? COLOR_OK : COLOR_BAD;
            case 22: return mValidator.validGloss(o.gloss, s) ? COLOR_OK : COLOR_BAD;
        }
        return COLOR_BAD;
    }
    return QVariant();
}

bool PendingModel::setData(const QModelIndex &index, const QVariant &value, int role) {
    if (!index.isValid() || index.row() < 0 || index.column() >= 23) {
        return false;
    }
    if (role != Qt::EditRole) {
        return false;
    }
    int row = index.row();
    int col = index.column();

    QString val = value.toString();

    PendingEntity &p = objects[row];
    switch(col) {
    case 0: if (p.accountNumber != val) {p.accountNumber = value.toString(); if(mListener) {mListener->onChanged();}} break;
    case 1: if (p.rutA != val) { p.rutA = value.toString(); if(mListener) {mListener->onChanged();}} break;
    case 2: if (p.enterpriseCode != val) { p.enterpriseCode = value.toString(); if(mListener) {mListener->onChanged();}} break;
    case 3: if (p.productCode != val) { p.productCode = value.toString(); if(mListener) {mListener->onChanged();}} break;
    case 4: if (p.datePost != val) { p.datePost = value.toString(); if(mListener) {mListener->onChanged();}} break;
    case 5: if (p.amountPost != val) { p.amountPost = value.toString(); if(mListener) {mListener->onChanged();}} break;
    case 6: if (p.dateContract != val) { p.dateContract = value.toString(); if(mListener) {mListener->onChanged();}} break;

    case 7: if (p.rutFather != val) { p.rutFather = value.toString(); if(mListener) {mListener->onChanged();}} break;
    case 8: if (p.fFirstName != val) { p.fFirstName = value.toString(); if(mListener) {mListener->onChanged();}} break;
    case 9: if (p.fLastName != val) { p.fLastName = value.toString(); if(mListener) {mListener->onChanged();}} break;
    case 10: if (p.rutMother != val) { p.rutMother = value.toString(); if(mListener) {mListener->onChanged();}} break;
    case 11: if (p.mFirstName != val) { p.mFirstName = value.toString(); if(mListener) {mListener->onChanged();}} break;
    case 12: if (p.mLastName != val) { p.mLastName = value.toString(); if(mListener) {mListener->onChanged();}} break;
    case 13: if (p.marriageDate != val) { p.marriageDate = value.toString(); if(mListener) {mListener->onChanged();}} break;
    case 14: if (p.childrenNumber != val) { p.childrenNumber = value.toString(); if(mListener) {mListener->onChanged();}} break;
    case 15: if (p.childrenNumberUnderAge != val) { p.childrenNumberUnderAge = value.toString(); if(mListener) {mListener->onChanged();}} break;
    case 16: if (p.address != val) { p.address = value.toString(); if(mListener) {mListener->onChanged();}} break;

    case 17: if (p.rutB != val) { p.rutB = value.toString(); if(mListener) {mListener->onChanged();}} break;
    case 18: if (p.firstName != val) { p.firstName = value.toString(); if(mListener) {mListener->onChanged();}} break;
    case 19: if (p.lastName != val) { p.lastName = value.toString(); if(mListener) {mListener->onChanged();}} break;
    case 20: if (p.actives != val) { p.actives = value.toString(); if(mListener) {mListener->onChanged();}} break;
    case 21: if (p.code != val) { p.code = value.toString(); if(mListener) {mListener->onChanged();}} break;
    case 22: if (p.gloss != val) { p.gloss = value.toString(); if(mListener) {mListener->onChanged();}} break;
    default:
        return false;
    }
    return true;
}
QVariant PendingModel::headerData(int section, Qt::Orientation orientation, int role) const {
    if (role != Qt::DisplayRole) {
        return QVariant();
    }
    if (orientation == Qt::Horizontal) {
        switch(section) {
        case 0: return "Numero de Cuenta";
        case 1: return "Rut Asociado";
        case 2: return "Codigo Empresa";
        case 3: return "Codigo Producto";
        case 4: return "Fecha Posteo";
        case 5: return "Monto Posteo";
        case 6: return "Fecha Contrato";

        case 7: return "Rut Padre";
        case 8: return "Nombres";
        case 9: return "Apellidos";
        case 10: return "Rut Madre";
        case 11: return "Nombres";
        case 12: return "Apellidos";
        case 13: return "Fecha Contrato Matrimonio";
        case 14: return "Numero de Hijos";
        case 15: return "Numero de Hijos menores de 18";
        case 16: return "Direccion";

        case 17: return "RUT";
        case 18: return "Nombre";
        case 19: return "Apellidos";
        case 20: return "Activos o Patrimonios";
        case 21: return "Codigo";
        case 22: return "Glosa";
        }
    }
    return QVariant();
}
Qt::ItemFlags PendingModel::flags(const QModelIndex &index) const {
    if (!index.isValid()) {
        return Qt::ItemIsEnabled;
    }
    return QAbstractTableModel::flags(index) ^ Qt::ItemIsEditable;
}

QList<PendingEntity> PendingModel::getList() const {
    return objects;
}

void PendingModel::setList(const QList<PendingEntity> &list) {
    beginResetModel();
    objects = list;
    endResetModel();
}

void PendingModel::setValidator(Validator validator) {
    mValidator = validator;
}
void PendingModel::setListener(PendingListener *listener) {
    mListener = listener;
}

void PendingModel::ignoreEntity(int i) {
    if (i < 0 || i >= objects.size()) {
        return;
    }
    PendingEntity &pe = objects[i];
    pe.accountNumber = "-";
    pe.rutA = "-";
    pe.enterpriseCode = "-";
    pe.productCode = "-";
    pe.datePost = "-";
    pe.amountPost = "-";
    pe.dateContract = "-";

    pe.rutFather = "-";
    pe.fFirstName = "-";
    pe.fLastName = "-";
    pe.rutMother = "-";
    pe.mFirstName = "-";
    pe.mLastName = "-";
    pe.marriageDate = "-";
    pe.childrenNumber = "-";
    pe.childrenNumberUnderAge = "-";
    pe.address = "-";

    pe.rutB = "-";
    pe.firstName = "-";
    pe.lastName = "-";
    pe.actives = "-";
    pe.code = "-";
    pe.gloss = "-";

    emit dataChanged(index(i, 0), index(i, 22));

    if(mListener) {
        mListener->onChanged();
    }
}

#include "injector.h"

Injector::Injector(ProgressHandler *ph) : progressHandler(ph) {

}
Injector::~Injector() {

}

void Injector::setup(FileDescriptorLanguage *fdl) {
    validator = Validator(fdl);
}

QString Injector::processFile(const QString &inputFilename, const QString &goodFilename,
        const QString &pendingPreffix, int itemsPerPage, int &countLines, int &numGoodLines,
        qulonglong &preTotalLines) {

    //------------------------------------- count lines -----

    QFile _tempFile(inputFilename);
    if (!_tempFile.open(QIODevice::ReadOnly)) {
        return "El archivo \"" + inputFilename + "\" no puede ser leido (existe?).";
    }

    progressHandler->onTitle("Contando lineas ...");
    qulonglong _linesCount = 0;
    {
        QTextStream input(&_tempFile);
        while(true) {
            QString temp = input.readLine();
            progressHandler->onUpdate(-1.0);
            if (temp.isNull()) {
                break;
            }
            _linesCount++;
        }
    }
    _tempFile.close();

    double partialPercent = 100.0/_linesCount;

    //-------------------------------------------------------

    QFile inputFile(inputFilename);
    if (!inputFile.open(QIODevice::ReadOnly)) {
        return "El archivo \"" + inputFilename + "\" no puede ser leido (existe?).";
    }

    QFile goodFile(goodFilename);
    if (!goodFile.open(QIODevice::WriteOnly)) {
        inputFile.close();
        return "El archivo \"" + goodFilename + "\" no puede ser escrito (permisos?).";
    }

    QTextStream in(&inputFile);
    QTextStream good(&goodFile);

    bool firstGood = true;
    QStringList temp;

    progressHandler->onTitle(QString("Creando archivos temporales ..."));

    CrossedEntity ce;
    QString reason;
    int temps = 0;
    countLines = 0;
    numGoodLines = 0;
    preTotalLines = 0;

    while(true) {
        QString line = in.readLine();
        if (line.isNull()) {
            break;
        }
        preTotalLines++;
        progressHandler->onUpdate(partialPercent * preTotalLines);
        if (validator.validLine(line.split("#"), reason, &ce)) {
            numGoodLines++;
            if (firstGood) {
                firstGood = false;
            } else {
                good << endl;
            }
            good << line;
        } else {
            temp.append(line);
            if (temp.size() == itemsPerPage * PAGE_FACTOR) {
                if (!writeToFile(QString("%1.%2.bin").arg(pendingPreffix).arg(temps), temp)) {
                    return "No se puede crear archivo temporal (permisos?)";
                }
                temp.clear();
                temps++;
                countLines += (itemsPerPage * PAGE_FACTOR);
            }
        }
    }

    if (temp.size() > 0) {
        progressHandler->onUpdate(-1);

        if (!writeToFile(QString("%1.%2.bin").arg(pendingPreffix).arg(temps), temp)) {
            return "No se puede crear archivo temporal (permisos?)";
        }
        countLines += temp.size();
        temp.clear();
        temps++;
    }

    inputFile.close();
    goodFile.close();

    return "";
}

QList<PendingEntity> Injector::getEntities(const QStringList &l, FileDescriptorLanguage *fdl) {
    QList<PendingEntity> result;
    foreach(QString s, l) {
        result.append(fdl->getPendingEntity(s.split("#")));
    }
    return result;
}

QString Injector::injectFromFiles(const QString &good, int numPendings, const QString &preffix,
        int insertsPerTransaction, qulonglong estimatedTotal) {

    // Prepare list of files to put in the transactions.
    QStringList filenames;
    filenames.append(good);
    for (int i = 0; i < numPendings; i++) {
        filenames.append(QString("%1.%2.bin").arg(preffix).arg(i));
    }

    QSqlDatabase db = getDb();
    if (!db.open()) {
        return "Error opening DB: " + db.lastError().text();
    }

    QStringList queries;

    qulonglong currentCount = 0;

    double partialTotal = 100.0 / estimatedTotal;

    int processedFiles = 0;
    int totalFiles = filenames.size();
    foreach (QString filename, filenames) {
        QFile file(filename);
        if (!file.exists()) {
            db.close();
            return "El archivo temporal " + file.fileName() + " no existe!";
        }
        processedFiles++;

        if (!file.open(QIODevice::ReadOnly)) {
            db.close();
            return "Error, el archivo temporal " + file.fileName() + " no puede ser abierto!";
        }

        progressHandler->onTitle(QString("Procesando archivo %1 / %2").arg(processedFiles).arg(totalFiles));

        QString reason;
        CrossedEntity ce;

        QTextStream in(&file);

        while(true) {
            QString line = in.readLine();
            if (line.isNull()) {
                break;
            }
            currentCount++;

            progressHandler->onUpdate(partialTotal * currentCount);

            QStringList parts = line.split("#");
            if (validator.validLine(parts, reason, &ce)) {
                queries.append(ce.getInsertQuery());

                // Execute buffered queries when limit reached.
                if (queries.size() >= insertsPerTransaction) {
                    QString status = executeTransaction(db, queries);
                    queries.clear();
                    if (!status.isEmpty()) {
                        db.close();
                        return status;
                    }
                }
            }
        }

        file.close();
        file.remove();
    }

    // Execute remaining queries.
    if (!queries.isEmpty()) {
        QString status = executeTransaction(db, queries);
        queries.clear();
        if (!status.isEmpty()) {
            db.close();
            return status;
        }
    }

    db.close();
    return "";
}

QString Injector::countLines(int numFiles, const QString &pendingPreffix, int &ignoredCount,
        int &correctedCount, int &invalidCount) {
    ignoredCount = 0;
    correctedCount = 0;
    invalidCount = 0;

    for (int i = 0; i < numFiles; i++) {
        QFile file(QString("%1.%2.bin").arg(pendingPreffix).arg(i));
        if (!file.exists()) {
            return "El archivo temporal " + file.fileName() + " no existe!";
        }
        if (!file.open(QIODevice::ReadOnly)) {
            return "Error, el archivo temporal " + file.fileName() + " no puede ser abierto!";
        }

        QTextStream in(&file);
        QString reason;
        QString line;
        while(true) {
            line = in.readLine();
            if (line.isNull()) {
                break;
            }
            QStringList parts = line.split("#");
            if (parts.count("-") == 23) {
                ignoredCount++;
            } else if (validator.validLine(parts, reason, NULL)) {
                correctedCount++;
            } else {
                invalidCount++;
            }
        }

        file.close();
    }

    return "";
}

QString Injector::executeTransaction(QSqlDatabase &db, const QStringList &queries) {
    QSqlQuery q(db);

    // Wrap all the inserts in a transaction

    q.exec("BEGIN;");
    if (q.lastError().isValid()) {
        return q.lastError().text();
    }

    foreach(QString query, queries) {
        q.exec(query);
        if (q.lastError().isValid()) {
            return q.lastError().text();
        }
    }

    q.exec("COMMIT;");
    if (q.lastError().isValid()) {
        return q.lastError().text();
    }

    return "";
}

bool Injector::writeToFile(const QString &filename, const QStringList &list) {
    QFile file(filename);
    if (!file.open(QIODevice::WriteOnly)) {
        return false;
    }
    QTextStream out(&file);
    bool first = true;
    foreach(QString s, list) {
        if (first) {
            first = false;
        } else {
            out << endl;
        }
        out << s;
    }
    file.close();
    return true;
}

QSqlDatabase Injector::getDb() {
    // load params and setup ui
    QSettings settings("settings.ini", QSettings::IniFormat);
    QString host = settings.value("host", "127.0.0.1").toString();
    QString username = settings.value("username", "").toString();
    QString password = settings.value("password", "").toString();
    int port = settings.value("port", 5432).toInt();
    QString database = settings.value("database", "earch_injector").toString();

    QSqlDatabase db;
    if (QSqlDatabase::contains("qt_sql_default_connection")) {
        db = QSqlDatabase::database("qt_sql_default_connection");
    } else {
        db = QSqlDatabase::addDatabase("QPSQL");
    }

    db.setHostName(host);
    db.setPort(port);
    db.setDatabaseName(database);
    db.setUserName(username);
    db.setPassword(password);
    db.setConnectOptions("sslmode=disable");
    return db;
}

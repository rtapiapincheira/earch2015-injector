#include "optionsdialog.h"

OptionsDialog::OptionsDialog(QWidget *parent) : QDialog(parent), ui(new Ui::OptionsDialog) {
    ui->setupUi(this);
    connect(ui->okButton, SIGNAL(clicked()), this, SLOT(okClicked()));
    connect(ui->cancelButton, SIGNAL(clicked()), this, SLOT(reject()));
    connect(ui->test_connection, SIGNAL(clicked()), this, SLOT(testConnectionClicked()));
    connect(ui->recreate_schema, SIGNAL(clicked()), this, SLOT(recreateSchemaClicked()));

    connect(ui->host_input, SIGNAL(textEdited(QString)), this, SLOT(onParameterChanged(QString)));
    connect(ui->port_input, SIGNAL(valueChanged(QString)), this, SLOT(onParameterChanged(QString)));
    connect(ui->username_input, SIGNAL(textEdited(QString)), this, SLOT(onParameterChanged(QString)));
    connect(ui->password_input, SIGNAL(textEdited(QString)), this, SLOT(onParameterChanged(QString)));
    connect(ui->database_input, SIGNAL(textEdited(QString)), this, SLOT(onParameterChanged(QString)));

    ui->error_label->setStyleSheet("QLabel {color : red; }");
}

OptionsDialog::~OptionsDialog() {
    delete ui;
}

int OptionsDialog::exec() {
    // load params and setup ui
    ui->error_label->setText("");

    QSettings settings("settings.ini", QSettings::IniFormat);

    ui->host_input->setText(settings.value("host", "127.0.0.1").toString());
    ui->username_input->setText(settings.value("username", "").toString());
    ui->password_input->setText(settings.value("password", "").toString());
    ui->port_input->setValue(settings.value("port", 5432).toInt());
    ui->database_input->setText(settings.value("database", "earch_injector").toString());

    onParameterChanged("");

    int code = QDialog::exec();
    if (code == QDialog::Accepted) {
        // save params
        settings.setValue("host", m_host);
        settings.setValue("username", m_username);
        settings.setValue("password", m_password);
        settings.setValue("port", m_port);
        settings.setValue("database", m_database);
    }
    return code;
}

bool OptionsDialog::validParameters() {
    ui->error_label->setText("");

    m_host = ui->host_input->text();
    m_username = ui->username_input->text();
    m_password = ui->password_input->text();
    m_port = ui->port_input->value();
    m_database = ui->database_input->text();

    if (QRegExp("\\d{1,3}.\\d{1,3}.\\d{1,3}.\\d{1,3}").indexIn(m_host) == -1) {
        ui->error_label->setText("Error: IP v4 invalido\n(ejemplo 255.255.255.255)");
        return false;
    }
    QStringList parts = m_host.split(".");
    if (parts.size() != 4) {
        ui->error_label->setText("Error: IP v4 invalido\n(ejemplo 255.255.255.255)");
        return false;
    }
    foreach(QString x, parts) {
        bool ok = false;
        int p = x.toInt(&ok);
        if (!ok) {
            ui->error_label->setText("IP v4 invalido, no numerico");
            return false;
        }
        if (p < 0 || p >= 256) {
            ui->error_label->setText("IP v4 invalido,\nsegmento fuera de rango [0,255]");
            return false;
        }
    }
    if (m_host.isEmpty()) {
        ui->error_label->setText("Nombre de host no puede ser vacio");
        return false;
    }
    if (m_username.isEmpty()) {
        ui->error_label->setText("Nombre de usuario no puede ser vacio");
        return false;
    }

    if (m_database.isEmpty()) {
        ui->error_label->setText("Nombre de base de datos no puede ser vacio");
        return false;
    }

    return true;
}

QSqlDatabase OptionsDialog::getDb() {
    QSqlDatabase db;
    if (QSqlDatabase::contains("qt_sql_default_connection")) {
        db = QSqlDatabase::database("qt_sql_default_connection");
    } else {
        db = QSqlDatabase::addDatabase("QPSQL");
    }
    db.setHostName(m_host);
    db.setPort(m_port);
    db.setDatabaseName(m_database);
    db.setUserName(m_username);
    db.setPassword(m_password);
    db.setConnectOptions("sslmode=disable");
    return db;
}

void OptionsDialog::okClicked() {
    if (validParameters()) {
        accept();
    }
}

void OptionsDialog::testConnectionClicked() {
    if (!validParameters()) {
        return;
    }
    QSqlDatabase db = getDb();
    if (db.open()) {
        QSqlQuery query(db);
        query.exec("select version()");
        QString result = query.lastError().text();
        while (query.next()) {
            result = query.value(0).toString();
            break;
        }
        QMessageBox::information(this, "Exito", result);
        ui->test_connection->setEnabled(false);
        ui->recreate_schema->setEnabled(true);
    } else {
        QMessageBox::critical(this, "Error", db.lastError().text());
    }
    db.close();
}

void OptionsDialog::recreateSchemaClicked() {
    if (!validParameters()) {
        return;
    }
    QFile file("data/schema.sql");
    if (file.open(QIODevice::ReadOnly)) {
        QTextStream in(&file);
        QSqlDatabase db = getDb();
        if (db.open()) {
            QSqlQuery query(db);
            while(true) {
                QString line = in.readLine();
                if (line.isNull()) {
                    break;
                } else if (!line.isEmpty() && !line.startsWith("--")) {
                    query.exec(line);
                }
            }
        } else {
            QMessageBox::critical(this, "Error", db.lastError().text());
        }
        db.close();

        file.close();
        ui->recreate_schema->setEnabled(false);
    } else {
        QMessageBox::critical(this, "Error", "Can't open file \"data/schema.sql\"");
    }
}

void OptionsDialog::onParameterChanged(const QString &newText) {
    Q_UNUSED(newText);
    if(!validParameters()) {
        return;
    }
    ui->test_connection->setEnabled(true);
    ui->recreate_schema->setEnabled(false);
}

#include "fdl.h"

QStringList FileDescriptorLanguage::VALIDATIONS = QStringList()
    << "UNSIGNED"
    << "RUT"
    << "ENTERPRISE_CODE"
    << "PRODUCT_CODE"
    << "DATE_YYYYMMDD"
    << "MONEY"
    << "NAME"
    << "ADDRESS"
    << "CODE"
    << "GLOSS";

void FileDescriptorLanguage::parseMapping(const QStringList &list) {
    mByTitle.clear();
    mByColumn.clear();
    mByIndex.clear();

    QRegExp fieldRegexp(MASTER_REGEXP);
    foreach(QString _it, list) {
        QString item = _it.trimmed();
        if (fieldRegexp.indexIn(item) != -1) {
            Field f;
            f.title = fieldRegexp.cap(1);
            f.column = fieldRegexp.cap(2);
            f.type = fieldRegexp.cap(3);

            mByTitle[f.title] = f;
            mByColumn[f.column] = f;
            mByIndex.append(f);
        }
    }
}

FileDescriptorLanguage::FileDescriptorLanguage(QFile *file)
    : mFile(file)
{
}

bool FileDescriptorLanguage::parse() {
    // No se puede parsear un archivo ya abierto!
    if (mFile->isOpen()) {
        return false;
    }

    if (mFile->open(QIODevice::ReadOnly | QIODevice::Text)) {
        QTextStream in(mFile);
        QString line = in.readLine();
        // No hay primera linea? no se puede parsear
        if (line.isNull()) {
            mFile->close();
            return false;
        }
        // Guardar en mFields la lista de campos con espacios al ppio/fin removidos.
        QStringList parts = line.split("#");
        int index = 0;
        foreach(QString s, parts) {
            mFields.append(s.trimmed());
            index++;
        }
        parseMapping(mFields);
        mFile->close();
        return true;
    }

    // Si no se puede abrir el archivo, no se puede parsear!
    return false;
}

QStringList FileDescriptorLanguage::getFields() const {
    return mFields;
}

int FileDescriptorLanguage::getFieldsSize() const {
    return mFields.size();
}

PendingEntity FileDescriptorLanguage::getPendingEntity(const QStringList &parts) {
    size_t size = parts.size();
    QVector<QString> q(size);
    for (size_t i = 0; i < size; i++) {
        QString col = mByIndex[i].column;
        int index = CrossedEntity::COLUMNS.indexOf(col);
        q[index] = parts[i];
    }
    return PendingEntity(q);
}

QStringList FileDescriptorLanguage::getAsLine(const PendingEntity &p) {
    QStringList parts = p.getParts();
    QStringList result;
    size_t size = parts.size();
    for (size_t i = 0; i < size; i++) {
        QString col = mByIndex[i].column;
        int index = CrossedEntity::COLUMNS.indexOf(col);
        result.append(parts[index]);
    }
    return result;
}

#include "querydialog.h"

const int QueryDialog::RESULTS_PER_PAGE = 10;

int QueryDialog::getTotalCount() {
    QSqlDatabase db = getDb();

    if (!db.open()) {
        QMessageBox::critical(this, "Error", "Error opening DB: " + db.lastError().text());
        return -1;
    }

    QSqlQuery query(db);
    query.exec("select count(*) as _total from crossing");
    if (query.lastError().isValid()) {
        QMessageBox::critical(this, "Error", "Error querying DB: " + db.lastError().text());
        return -1;
    }
    QString result;
    while (query.next()) {
        result = query.value(0).toString();
        break;
    }

    db.close();
    bool ok;
    int count = result.toInt(&ok);
    if (!ok) {
        QMessageBox::critical(this, "Error", "Can't retrieve rows count from DB (" + result + ")");
        return -1;
    }
    return (ok ? count : -1);
}

QList<CrossedEntity> getCrossedEntities(QSqlQuery &q) {
    QList<CrossedEntity> result;
    while(q.next()) {
        result.append(CrossedEntity(q));
    }
    return result;
}

void QueryDialog::update(int currentPage) {

    // currentPage es un entero, que represetan la pagina (de a RESULTS_PER_PAGE elementos).

    int count = getTotalCount();
    int totalPages = (count % RESULTS_PER_PAGE == 0
            ? count/RESULTS_PER_PAGE : count/RESULTS_PER_PAGE+1);
    int lastPage = totalPages-1;

    if (count > 0) {
        ui->label->setText(QString("%1 / %2").arg(currentPage+1).arg(totalPages));
    } else {
        ui->label->setText("No data");
    }

    if (count < 0) {
        return;
    }

    ui->first_button->setEnabled(currentPage != 0);
    ui->previous_button->setEnabled(currentPage > 0);
    ui->next_button->setEnabled(currentPage < lastPage);
    ui->last_button->setEnabled(currentPage < lastPage);

    QString offset = QString::number(RESULTS_PER_PAGE*currentPage);
    QString limit = QString::number(RESULTS_PER_PAGE);

    QSqlDatabase db = getDb();
    if (!db.open()) {
        QMessageBox::critical(this, "Error", "Error opening DB: " + db.lastError().text());
        return;
    }

    QSqlQuery q(db);
    QString stringQuery = "select * from crossing order by rut_a asc, rut_b asc, rut_father asc, rut_mother asc NULLS first limit " + limit + " offset " + offset;

    q.exec(stringQuery);
    QString lastError = q.lastError().text().trimmed();
    if (!lastError.isEmpty()) {
        QMessageBox::critical(this, "Error", lastError);
        return;
    }

    crossed_model->setList(getCrossedEntities(q));
}

QueryDialog::QueryDialog(QWidget *parent) : QDialog(parent), ui(new Ui::QueryDialog), m_page(0) {
    ui->setupUi(this);
    connect(ui->okButton, SIGNAL(clicked()), this, SLOT(accept()));

    connect(ui->first_button, SIGNAL(clicked()), this, SLOT(onFirstClicked()));
    connect(ui->previous_button, SIGNAL(clicked()), this, SLOT(onPreviousClicked()));
    connect(ui->next_button, SIGNAL(clicked()), this, SLOT(onNextClicked()));
    connect(ui->last_button, SIGNAL(clicked()), this, SLOT(onLastClicked()));

    crossed_model = new CrossedModel;
    ui->results_table->setModel(crossed_model);
}

QueryDialog::~QueryDialog() {
    delete crossed_model;
    delete ui;
}

int QueryDialog::exec() {
    // load params and setup ui
    QSettings settings("settings.ini", QSettings::IniFormat);
    m_host = settings.value("host", "127.0.0.1").toString();
    m_username = settings.value("username", "").toString();
    m_password = settings.value("password", "").toString();
    m_port = settings.value("port", 5432).toInt();
    m_database = settings.value("database", "earch_injector").toString();

    update(m_page = 0);

    int code = QDialog::exec();
    if (code == QDialog::Accepted) {
        // save params
    }
    return code;
}

QSqlDatabase QueryDialog::getDb() {
    QSqlDatabase db;
    if (QSqlDatabase::contains("qt_sql_default_connection")) {
        db = QSqlDatabase::database("qt_sql_default_connection");
    } else {
        db = QSqlDatabase::addDatabase("QPSQL");
    }

    db.setHostName(m_host);
    db.setPort(m_port);
    db.setDatabaseName(m_database);
    db.setUserName(m_username);
    db.setPassword(m_password);
    db.setConnectOptions("sslmode=disable");
    return db;
}

void QueryDialog::onFirstClicked() {
    update(m_page = 0);
}
void QueryDialog::onPreviousClicked() {
    update(--m_page);
}
void QueryDialog::onNextClicked() {
    update(++m_page);
}
void QueryDialog::onLastClicked() {
    int count = getTotalCount();
    if (count >= 0) {
        if (count % RESULTS_PER_PAGE == 0) {
            update(m_page = count/RESULTS_PER_PAGE-1);
        } else {
            update(m_page = count/RESULTS_PER_PAGE);
        }
    }
}

#include "entities.h"

QStringList CrossedEntity::COLUMNS = QStringList()
    << "account_number"
    << "rut_a"
    << "enterprise_code"
    << "product_code"
    << "date_post"
    << "amount_post"
    << "date_contract"

    << "rut_father"
    << "f_first_name"
    << "f_last_name"
    << "rut_mother"
    << "m_first_name"
    << "m_last_name"
    << "marriage_date"
    << "children_number"
    << "children_number_under_age"
    << "address"

    << "rut_b"
    << "first_name"
    << "last_name"
    << "actives"
    << "code"
    << "gloss"
    ;

CrossedEntity::CrossedEntity() {
}

CrossedEntity::CrossedEntity(QSqlQuery &q) {
    accountNumber = q.value(COLUMNS[0]).toUInt();
    rutA = q.value(COLUMNS[1]).toUInt();
    enterpriseCode = q.value(COLUMNS[2]).toString();
    productCode = q.value(COLUMNS[3]).toString();
    datePost = q.value(COLUMNS[4]).toDate();
    amountPost = q.value(COLUMNS[5]).toUInt();
    dateContract = q.value(COLUMNS[6]).toDate();

    rutFather = q.value(COLUMNS[7]).toUInt();
    fFirstName = q.value(COLUMNS[8]).toString();
    fLastName = q.value(COLUMNS[9]).toString();
    rutMother = q.value(COLUMNS[10]).toUInt();
    mFirstName = q.value(COLUMNS[11]).toString();
    mLastName = q.value(COLUMNS[12]).toString();
    marriageDate = q.value(COLUMNS[13]).toDate();
    childrenNumber = q.value(COLUMNS[14]).toUInt();
    childrenNumberUnderAge = q.value(COLUMNS[15]).toUInt();
    address = q.value(COLUMNS[16]).toString();

    rutB = q.value(COLUMNS[17]).toUInt();
    firstName = q.value(COLUMNS[18]).toString();
    lastName = q.value(COLUMNS[19]).toString();
    actives = q.value(COLUMNS[20]).toUInt();
    code = q.value(COLUMNS[21]).toString();
    gloss = q.value(COLUMNS[22]).toString();
}

void CrossedEntity::assignByField(const Field &f, const QString &s) {
    int index = COLUMNS.indexOf(f.column);

    switch (index) {
    case 0: accountNumber = s.toUInt(); break;
    case 1: rutA = s.toUInt(); break;
    case 2: enterpriseCode = s; break;
    case 3: productCode = s; break;
    case 4: datePost = QDate::fromString(s, "yyyyMMdd"); break;
    case 5: amountPost = s.toUInt(); break;
    case 6: dateContract = QDate::fromString(s, "yyyyMMdd"); break;

    case 7: rutFather = s.toUInt(); break;
    case 8: fFirstName = s; break;
    case 9: fLastName = s; break;
    case 10: rutMother = s.toUInt(); break;
    case 11: mFirstName = s; break;
    case 12: mLastName = s; break;
    case 13: marriageDate = QDate::fromString(s, "yyyyMMdd"); break;
    case 14: childrenNumber = s.toUInt(); break;
    case 15: childrenNumberUnderAge = s.toUInt(); break;
    case 16: address = s; break;

    case 17: rutB = s.toUInt(); break;
    case 18: firstName = s; break;
    case 19: lastName = s; break;
    case 20: actives = s.toUInt(); break;
    case 21: code = s; break;
    case 22: gloss = s; break;
    }
}

QString CrossedEntity::escape(const QString &s) {
    QString t = s;
    return t.replace("'", "''");
}

QString CrossedEntity::getInsertQuery() const {
    static QString columnPart = "";
    if (columnPart.isEmpty()) {
        columnPart = COLUMNS.join(",");
    }

    return QString("insert into crossing (%24) values("
        "'%1','%2','%3','%4','%5','%6','%7','%8','%9','%10',"
        "'%11','%12','%13','%14','%15','%16','%17','%18','%19','%20',"
        "'%21','%22','%23'"
        ");")
        .arg(accountNumber)
        .arg(rutA)
        .arg(escape(enterpriseCode))
        .arg(escape(productCode))
        .arg(datePost.toString("yyyyMMdd"))
        .arg(amountPost)
        .arg(dateContract.toString("yyyyMMdd"))

        .arg(rutFather)
        .arg(escape(fFirstName))
        .arg(escape(fLastName))
        .arg(rutMother)
        .arg(escape(mFirstName))
        .arg(escape(mLastName))
        .arg(marriageDate.toString("yyyyMMdd"))
        .arg(childrenNumber)
        .arg(childrenNumberUnderAge)
        .arg(escape(address))

        .arg(rutB)
        .arg(escape(firstName))
        .arg(escape(lastName))
        .arg(actives)
        .arg(escape(code))
        .arg(escape(gloss))

        .arg(columnPart)
    ;
}

//--------------------------------------------------------------------------------------------------

bool PendingEntity::isIgnored() const {
    return
    accountNumber == "-" &&
    rutA == "-" &&
    enterpriseCode == "-" &&
    productCode == "-" &&
    datePost == "-" &&
    amountPost == "-" &&
    dateContract == "-" &&

    rutFather == "-" &&
    fFirstName == "-" &&
    fLastName == "-" &&
    rutMother == "-" &&
    mFirstName == "-" &&
    mLastName == "-" &&
    marriageDate == "-" &&
    childrenNumber == "-" &&
    childrenNumberUnderAge == "-" &&
    address == "-" &&

    rutB == "-" &&
    firstName == "-" &&
    lastName == "-" &&
    actives == "-" &&
    code == "-" &&
    gloss == "-";
}

PendingEntity::PendingEntity() {
}

PendingEntity::PendingEntity(const QVector<QString> &parts) {
    accountNumber = parts[0];
    rutA = parts[1];
    enterpriseCode = parts[2];
    productCode = parts[3];
    datePost = parts[4];
    amountPost = parts[5];
    dateContract = parts[6];

    rutFather = parts[7];
    fFirstName = parts[8];
    fLastName = parts[9];
    rutMother = parts[10];
    mFirstName = parts[11];
    mLastName = parts[12];
    marriageDate = parts[13];
    childrenNumber = parts[14];
    childrenNumberUnderAge = parts[15];
    address = parts[16];

    rutB = parts[17];
    firstName = parts[18];
    lastName = parts[19];
    actives = parts[20];
    code = parts[21];
    gloss = parts[22];
}

CrossedEntity PendingEntity::toCrossedEntity() {
    CrossedEntity c;

    c.accountNumber = accountNumber.toUInt();
    c.rutA = rutA.toUInt();
    c.enterpriseCode = enterpriseCode;
    c.productCode = productCode;
    c.datePost = QDate::fromString(datePost, "yyyyMMdd");
    c.amountPost = amountPost.replace("$","").replace(".","").toUInt();
    c.dateContract = QDate::fromString(dateContract, "yyyyMMdd");

    c.rutFather = rutFather.toUInt();
    c.fFirstName = fFirstName;
    c.fLastName = fLastName;
    c.rutMother = rutMother.toUInt();
    c.mFirstName = mFirstName;
    c.mLastName = mLastName;
    c.marriageDate = QDate::fromString(marriageDate, "yyyyMMdd");
    c.childrenNumber = childrenNumber.toUInt();
    c.childrenNumberUnderAge = childrenNumberUnderAge.toUInt();
    c.address = address;

    c.rutB = rutB.toUInt();
    c.firstName = firstName;
    c.lastName = lastName;
    c.actives = actives.replace("$","").replace(".","").toUInt();
    c.code = code;
    c.gloss = gloss;

    return c;
}

QStringList PendingEntity::getParts() const {
    return QStringList()
        << accountNumber
        << rutA
        << enterpriseCode
        << productCode
        << datePost
        << amountPost
        << dateContract

        << rutFather
        << fFirstName
        << fLastName
        << rutMother
        << mFirstName
        << mLastName
        << marriageDate
        << childrenNumber
        << childrenNumberUnderAge
        << address

        << rutB
        << firstName
        << lastName
        << actives
        << code
        << gloss
    ;
}

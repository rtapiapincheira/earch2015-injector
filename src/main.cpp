#include "mainwindow.h"
#include <QApplication>

#include "injector.h"
#include "validator.h"

int main(int argc, char **argv) {
    QApplication a(argc, argv);

    MainWindow window;
    window.loadParameters();
    window.show();
    int code = a.exec();
    window.saveParameters();
    return code;
}

#include "validator.h"

Validator::Validator() {
}

Validator::Validator(FileDescriptorLanguage *fdl) {
    mFdl = fdl;
}
Validator::~Validator() {

}

bool Validator::validLine(const QStringList &parts, QString &reason, CrossedEntity *c) {
    QString s;

    CrossedEntity dummy;
    CrossedEntity &e = (c ? *c : dummy);

    size_t size = mFdl->mByIndex.size();
    for (size_t i = 0; i < size; i++) {
        const QString &p = parts[i];
        const Field &f = mFdl->mByIndex[i];
        if (f.type == "UNSIGNED") {
            if (!validUnsignedInteger(p, s)) {
                reason = "Invalid UNSIGNED:" + p;
                return false;
            }
        }
        else if (f.type == "RUT") {
            if (!validRut(p, s)) {
                reason = "Invalid RUT:" + p;
                return false;
            }
        }
        else if (f.type == "ENTERPRISE_CODE") {
            if (!validEnterpriseCode(p, s)) {
                reason = "Invalid ENTERPRISE_CODE:" + p;
                return false;
            }
        }
        else if (f.type == "PRODUCT_CODE") {
            if (!validProductCode(p, s)) {
                reason = "Invalid PRODUCT_CODE:" + p;
                return false;
            }
        }
        else if (f.type == "DATE_YYYYMMDD") {
            if (!validDate(p, s)) {
                reason = "Invalid DATE_YYYYMMDD:" + p;
                return false;
            }
        }
        else if (f.type == "MONEY") {
            if (!validMoneyAmount(p, s)) {
                reason = "Invalid MONEY:" + p;
                return false;
            }
        }
        else if (f.type == "NAME") {
            if (!validName(p, s)) {
                reason = "Invalid NAME:" + p;
                return false;
            }
        }
        else if (f.type == "ADDRESS") {
            if (!validAddress(p, s)) {
                reason = "Invalid ADDRESS:" + p;
                return false;
            }
        }
        else if (f.type == "CODE") {
            if (!validSiiCode(p, s)) {
                reason = "Invalid CODE:" + p;
                return false;
            }
        }
        else if (f.type == "GLOSS") {
            if (!validGloss(p, s)) {
                reason = "Invalid GLOSS:" + p;
                return false;
            }
        }
        e.assignByField(f, s);
    }

    return true;
}

bool Validator::validFdl(FileDescriptorLanguage *fdl) {
    QString reason;
    return validFdl(fdl, reason);
}

bool Validator::validFdl(FileDescriptorLanguage *fdl, QString &reason) {
    QRegExp fieldRegexp(MASTER_REGEXP);
    foreach(QString _it, fdl->getFields()) {
        QString item = _it.trimmed();
        if (fieldRegexp.indexIn(item) == -1) {
            reason = "Invalid FDL field: \"" + item + "\" must match title<column:type>";
            return false;
        }
        QString myType = fieldRegexp.cap(3);
        if (FileDescriptorLanguage::VALIDATIONS.indexOf(myType) < 0) {
            reason = "Invalid type: " + myType;
            return false;
        }
    }
    return true;
}

QString Validator::getDv(int num) const {
    std::string rutS = QString::number(num).toStdString();

    int factor = 2;
    int calculoDV = 0;
    for (int i = rutS.size()-1; i >= 0; i--) {
        calculoDV += (rutS[i] - '0') * factor;
        factor++;
        if (factor == 8){
            factor = 2;
        }
    }
    calculoDV = 11 - ( calculoDV % 11);
    return (calculoDV == 10
    ? "K" : (calculoDV == 11
             ? "0" : QString::number(calculoDV))
    );
}

bool Validator::validRut(const QString &rut, QString &sanitized) const {
    QStringList parts = rut.split("-");
    if (parts.size() != 2) {
        return false;
    }
    bool ok;
    sanitized = parts[0].replace(".", "");
    quint32 num = sanitized.toUInt(&ok);
    if (!ok) {
        return false;
    }
    QString expectedDv = getDv(num);
    ok = (expectedDv.toLower() == parts[1].trimmed().toLower());
    return ok;
}
bool Validator::validEnterpriseCode(const QString &enterpriseCode, QString &sanitized) const {
    QString trimmed = enterpriseCode.trimmed().toUpper();
    if (!trimmed.isEmpty()) {
        sanitized = trimmed;
        return true;
    }
    return false;
}
bool Validator::validProductCode(const QString &productCode, QString &sanitized) const {
    QString trimmed = productCode.trimmed().toUpper();
    if (!trimmed.isEmpty()) {
        sanitized = trimmed;
        return true;
    }
    return false;
}
bool Validator::validDate(const QString &date, QString &sanitized) const {
    QString trimmed = date.trimmed();
    QDate aDate = QDate::fromString(trimmed, "yyyyMMdd");
    if (aDate.isValid()) {
        sanitized = trimmed;
        return true;
    }
    return false;
}
bool Validator::validMoneyAmount(const QString &moneyAmount, QString &sanitized) const {
    QString trimmed = moneyAmount.trimmed();
    if (trimmed.isEmpty()) {
        return false;
    }
    bool ok;
    sanitized = trimmed.replace("$", "").replace(".", "");
    quint32 intVal = sanitized.toUInt(&ok);
    if (!ok) {
        return false;
    }
    return true;
}

bool Validator::validName(const QString &firstName, QString &sanitized) const {
    QString trimmed = firstName.trimmed().toUpper();
    if (trimmed.isEmpty()) {
        return false;
    }
    QRegExp nameRegexp("([áéíóúüÁÉÍÓÚÜ\\sa-zA-ZñÑ])+");
    if (nameRegexp.indexIn(trimmed) == -1) {
        return false;
    }
    sanitized = trimmed;
    return true;
}

bool Validator::validUnsignedInteger(const QString &value, QString &sanitized) const {
    QString trimmed = value.trimmed();
    if (trimmed.isEmpty()) {
        return false;
    }
    bool ok;
    quint32 val = trimmed.toUInt(&ok);
    Q_UNUSED(val);
    if (ok) {
        sanitized = trimmed;
    }
    return ok;
}
bool Validator::validAddress(const QString &address, QString &sanitized) const {
    QString trimmed = address.trimmed().toUpper();
    if (trimmed.isEmpty()) {
        return false;
    }
    QRegExp addressRegexp("^[áéíóúüÁÉÍÓÚÜ\\sa-zA-Z0-9ñÑ\\.:\\\\,#\\-'/]+$");
    if (addressRegexp.indexIn(trimmed) == -1) {
        return false;
    }
    sanitized = trimmed;
    return true;
}

bool Validator::validSiiCode(const QString &siiCode, QString &sanitized) const {
    QString trimmed = siiCode.trimmed().toUpper();
    if (trimmed.isEmpty()) {
        return false;
    }
    sanitized = trimmed;
    return true;
}
bool Validator::validGloss(const QString &gloss, QString &sanitized) const {
    QString trimmed = gloss.trimmed();
    if (trimmed.isEmpty()) {
        return false;
    }
    sanitized = trimmed;
    return true;
}

bool Validator::validPendingEntity(const PendingEntity &p) const {
    QString s;
    return
        validUnsignedInteger(p.accountNumber, s) &&
        validUnsignedInteger(p.rutA, s) &&
        validEnterpriseCode(p.enterpriseCode, s) &&
        validProductCode(p.productCode, s) &&
        validDate(p.datePost, s) &&
        validUnsignedInteger(p.amountPost, s) &&
        validDate(p.dateContract, s) &&

        validUnsignedInteger(p.rutFather, s) &&
        validName(p.fFirstName, s) &&
        validName(p.fLastName, s) &&
        validUnsignedInteger(p.rutMother, s) &&
        validName(p.mFirstName, s) &&
        validName(p.mLastName, s) &&
        validDate(p.marriageDate, s) &&
        validUnsignedInteger(p.childrenNumber, s) &&
        validUnsignedInteger(p.childrenNumberUnderAge, s) &&
        validAddress(p.address, s) &&

        validUnsignedInteger(p.rutFather, s) &&
        validName(p.fFirstName, s) &&
        validName(p.fLastName, s) &&
        validMoneyAmount(p.actives, s) &&
        validSiiCode(p.code, s) &&
        validGloss(p.gloss, s);
}

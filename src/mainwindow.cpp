#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent), ui(new Ui::MainWindow), mFdl(NULL), injector(this), currentPage(0) {

    ui->setupUi(this);

    connect(ui->actionOpenFile, SIGNAL(triggered()), this, SLOT(onOpenFileClicked()));
    connect(ui->actionOpenFdl, SIGNAL(triggered()), this, SLOT(onOpenFdlClicked()));
    connect(ui->actionExit, SIGNAL(triggered()), this, SLOT(onExitClicked()));
    connect(ui->actionQuery, SIGNAL(triggered()), this, SLOT(onQueryClicked()));
    connect(ui->actionOptions, SIGNAL(triggered()), this, SLOT(onOptionsClicked()));

    connect(ui->actionClean, SIGNAL(triggered()), this, SLOT(onCleanClicked()));

    connect(ui->ignore_button, SIGNAL(clicked()), this, SLOT(onIgnoreClicked()));
    connect(ui->process_file_button, SIGNAL(clicked()), this, SLOT(onProcessFileClicked()));
    connect(ui->saveCorrections_button, SIGNAL(clicked()), this, SLOT(onSaveCorrectionsClicked()));
    connect(ui->insert_batch_button, SIGNAL(clicked()), this, SLOT(onInsertBatchClicked()));

    connect(ui->next_button, SIGNAL(clicked()), this, SLOT(onNextClicked()));
    connect(ui->last_button, SIGNAL(clicked()), this, SLOT(onLastClicked()));
    connect(ui->first_button, SIGNAL(clicked()), this, SLOT(onFirstClicked()));
    connect(ui->previous_button, SIGNAL(clicked()), this, SLOT(onPreviousClicked()));

    connect(ui->results_table, SIGNAL(clicked(QModelIndex)), this, SLOT(onResultsTableClicked(const QModelIndex &)));

    pending_model = new PendingModel;
    pending_model->setListener(this);
    ui->results_table->setModel(pending_model);
}

MainWindow::~MainWindow() {
    delete ui;
    if (mFdl) {
        delete mFdl;
    }
    if (pending_model) {
        delete pending_model;
    }
}

void MainWindow::updateButtons() {
    bool ready2process = (!ui->file_label->text().isEmpty() && !ui->fdl_label->text().isEmpty());
    ui->process_file_button->setEnabled(ready2process);
}

void MainWindow::loadParameters() {
    QSettings s("settings.ini", QSettings::IniFormat);
    ui->file_label->setText(s.value("file").toString());
    ui->fdl_label->setText(s.value("fdl").toString());
    updateButtons();
}
void MainWindow::saveParameters() {
    QSettings s("settings.ini", QSettings::IniFormat);
    s.setValue("file", ui->file_label->text());
    s.setValue("fdl", ui->fdl_label->text());
}

QStringList MainWindow::getSlice(int page) {
    int fileIndex = page/PAGE_FACTOR;
    QFile file(QString("%1.%2.bin").arg(PENDING_PREFFIX).arg(fileIndex));
    if (!file.exists()) {
        QMessageBox::warning(this, "Error", "El archivo temporal " + file.fileName() + " no existe!");
        return QStringList();
    }
    if (!file.open(QIODevice::ReadOnly)) {
        QMessageBox::warning(this, "Error", "El archivo temporal " + file.fileName() + " no puede ser abierto!");
        return QStringList();
    }

    int block = page % PAGE_FACTOR;

    QTextStream in(&file);
    QStringList result;
    int first = block*ITEMS_PER_PAGE;
    int last = first+ITEMS_PER_PAGE-1;

    int index = 0;
    while(true) {
        QString line = in.readLine();
        if (line.isNull()) {
            break;
        }
        if (first <= index){
            result.append(line);
        }
        if (index >= last) {
            break;
        }
        index++;
    }

    return result;
}

void MainWindow::loadPage(int page) {
    QStringList lines = getSlice(page);
    QList<PendingEntity> entities = injector.getEntities(lines, mFdl);

    pending_model->setList(entities);

    int totalPages = mCountLines%ITEMS_PER_PAGE == 0
            ? mCountLines/ITEMS_PER_PAGE : mCountLines/ITEMS_PER_PAGE+1;
    int lastPage = (mCountLines % ITEMS_PER_PAGE == 0
            ? mCountLines/ITEMS_PER_PAGE-1 : mCountLines/ITEMS_PER_PAGE);

    ui->first_button->setEnabled(currentPage > 0);
    ui->previous_button->setEnabled(currentPage > 0);
    ui->next_button->setEnabled(currentPage < lastPage);
    ui->last_button->setEnabled(currentPage < lastPage);

    ui->page_label->setEnabled(true);
    if (mCountLines == 0) {
        ui->page_label->setText("No data");
    } else {
        ui->page_label->setText(QString("%1 / %2").arg(page+1).arg(totalPages));
    }
}

void MainWindow::savePage(int page, const QStringList &lines) {
    int fileIndex = page/PAGE_FACTOR;
    QFile file(QString("%1.%2.bin").arg(PENDING_PREFFIX).arg(fileIndex));
    if (!file.exists()) {
        QMessageBox::warning(this, "Error", "El archivo temporal " + file.fileName() + " no existe!");
        return;
    }

    if (!file.open(QIODevice::ReadOnly)) {
        QMessageBox::warning(this, "Error", "El archivo temporal " + file.fileName() + " no puede ser abierto!");
        return;
    }

    int block = page % PAGE_FACTOR;
    int first = block * ITEMS_PER_PAGE;
    int last = first + ITEMS_PER_PAGE - 1;

    QTextStream in(&file);

    QStringList allLines;
    while(true) {
        QString line = in.readLine();
        if (line.isNull()) {
            break;
        }
        allLines.append(line);
    }
    file.close();

    int k = 0;
    for (int i = first; i <= last && k < lines.size(); i++, k++) {
        allLines[i] = lines[k];
    }

    if (!file.open(QIODevice::WriteOnly)) {
        QMessageBox::warning(this, "Error", "El archivo temporal " + file.fileName() + " no puede ser escrito!");
        return;
    }

    QTextStream out(&file);
    bool firstWritten = true;
    foreach(QString s, allLines) {
        if (firstWritten) {
            firstWritten = false;
        } else {
            out << endl;
        }
        out << s;
    }
    file.close();
}
void MainWindow::onChanged() {
    ui->saveCorrections_button->setEnabled(true);
}

void MainWindow::onUpdate(double percent) {
    if (percent >= 0 && percent <= 100.0) {
        ui->progress->setValue((int)percent);
    }
    qApp->processEvents();
}
void MainWindow::onTitle(const QString &title) {
    ui->title->setText(title);
    qApp->processEvents();
}

void MainWindow::onOpenFileClicked() {
    QString filename = QFileDialog::getOpenFileName(this, "Archivo de datos", ".",
            "Archivos de datos (*.txt *.csv *.dat)");
    if (!filename.isEmpty()) {
        ui->file_label->setText(filename);
    }
    updateButtons();
}
void MainWindow::onOpenFdlClicked() {
    QString filename = QFileDialog::getOpenFileName(this, "Archivo FDL", ".",
            "Archivos de datos (*.txt *.csv *.dat *.fdl)");
    if (!filename.isEmpty()) {
        ui->fdl_label->setText(filename);
    }
    updateButtons();
}
void MainWindow::onExitClicked() {
    QCoreApplication::quit();
}
void MainWindow::onQueryClicked() {
    QueryDialog dialog;
    dialog.exec();
}
void MainWindow::onOptionsClicked() {
    OptionsDialog dialog;
    dialog.exec();
}
void MainWindow::onCleanClicked() {
    ui->file_label->setText("");
    ui->fdl_label->setText("");
    ui->valid_label->setText("");
    ui->lines_label->setText("");

    updateButtons();
}

void MainWindow::onProcessFileClicked() {
    QString filename = ui->file_label->text();
    QFile file(filename);
    if (!file.exists()) {
        QMessageBox::warning(this, "Error", "El archivo de cruce \"" + filename + "\" no existe.");
        return;
    }

    QString fdlFilename = ui->fdl_label->text();
    QFile fileFdl(fdlFilename);
    if (!fileFdl.exists()) {
        QMessageBox::warning(this, "Error", "El archivo FDL \"" + fdlFilename + "\" de cruce no existe.");
        return;
    }
    mFdl = new FileDescriptorLanguage(&fileFdl);
    if (!mFdl->parse()) {
        QMessageBox::warning(this, "Error", "El archivo FDL \"" + fdlFilename + "\" es invalido.");
        return;
    }

    ui->progress->setEnabled(true);
    ui->process_file_button->setEnabled(false);

    pending_model->setValidator(Validator(mFdl));
    injector.setup(mFdl);
    mPreTotalLines = 0;
    QString msg = injector.processFile(ui->file_label->text(), GOOD_FILE, PENDING_PREFFIX,
            ITEMS_PER_PAGE, mCountLines, mGoodLines, mPreTotalLines);
    if (msg.isEmpty()) {
        ui->insert_batch_button->setEnabled(true);
        ui->lines_label->setText(QString::number(mCountLines+mGoodLines));
        ui->valid_label->setText(QString("%1 / %2").arg(mGoodLines).arg(mCountLines+mGoodLines));
        if (mCountLines > 0) {
            loadPage(0);
        }
        ui->process_file_button->setEnabled(false);
        ui->title->setText("Procesamiento OK");
    } else {
        QMessageBox::warning(this, "Error", msg);
        ui->title->setText(msg);
    }
    ui->progress->setValue(0);
    ui->progress->setEnabled(false);
}
void MainWindow::onInsertBatchClicked() {

    int ignoredCount = 0;
    int correctedCount = 0;
    int invalidCount = 0;

    // Number of pending temporary files.
    int numPages = (mCountLines % ITEMS_PER_PAGE == 0
            ? mCountLines/ITEMS_PER_PAGE : mCountLines/ITEMS_PER_PAGE+1);
    int numFiles = (numPages % PAGE_FACTOR == 0 ? numPages/PAGE_FACTOR : numPages/PAGE_FACTOR+1);

    injector.countLines(numFiles, PENDING_PREFFIX, ignoredCount, correctedCount, invalidCount);

    QString msg = "Se insertarán:\n\n" +
            QString::number(mGoodLines) + " registros OK\n" +
            QString::number(correctedCount) + " registros corregidos\n" +
            QString::number(ignoredCount) + " registros serán ignorados\n" +
            QString::number(invalidCount) + " registros inválidos serán omitidos\n\n"
            "Continuar?";

    // Insertar solo las lineas desde good.txt
    int code = QMessageBox::question(this, "Confirmacion", msg);
    if (code == QMessageBox::Yes) {

        ui->insert_batch_button->setEnabled(false);
        ui->first_button->setEnabled(false);
        ui->previous_button->setEnabled(false);
        ui->next_button->setEnabled(false);
        ui->last_button->setEnabled(false);
        ui->ignore_button->setEnabled(false);
        ui->saveCorrections_button->setEnabled(false);

        ui->progress->setEnabled(true);

        // proceed
        QString status = injector.injectFromFiles(GOOD_FILE, numFiles, PENDING_PREFFIX,
                INSERTS_PER_TRANSACTION, mPreTotalLines);
        if (status.isEmpty()) {

            ui->file_label->setText("");
            ui->fdl_label->setText("");
            ui->process_file_button->setEnabled(true);

            ui->insert_batch_button->setEnabled(false);
            ui->ignore_button->setEnabled(false);
            ui->saveCorrections_button->setEnabled(false);
            pending_model->setList(QList<PendingEntity>());
            ui->page_label->setText("No data");

            ui->first_button->setEnabled(false);
            ui->previous_button->setEnabled(false);
            ui->next_button->setEnabled(false);
            ui->last_button->setEnabled(false);

            ui->lines_label->setText("");
            ui->valid_label->setText("");

            ui->title->setText("Inyeccion OK");
            ui->progress->setValue(0);
            ui->progress->setEnabled(false);
            QMessageBox::information(this, "Info", "Inyeccion OK");

        } else {
            ui->title->setText(status);
            ui->progress->setValue(0);
            ui->progress->setEnabled(false);
            QMessageBox::warning(this, "Error", status);
        }
    }
}
void MainWindow::onIgnoreClicked() {
    QModelIndex mi = ui->results_table->currentIndex();
    if (!mi.isValid()) {
        return;
    }
    int row = mi.row();
    int size = pending_model->getList().size();
    if (row < 0 || row >= size) {
        return;
    }
    pending_model->ignoreEntity(row);
}
void MainWindow::onSaveCorrectionsClicked() {
    ui->saveCorrections_button->setEnabled(false);
    QList<PendingEntity> pendings = pending_model->getList();
    QStringList lines;
    foreach(PendingEntity pe, pendings) {
        QStringList ql = mFdl->getAsLine(pe);
        lines.append(ql.join("#"));
    }
    savePage(currentPage, lines);
}

void MainWindow::onFirstClicked() {
    loadPage(currentPage = 0);
}
void MainWindow::onPreviousClicked() {
    if (currentPage > 0) {
        loadPage(--currentPage);
    }
}
void MainWindow::onNextClicked() {
    if (currentPage < mCountLines/ITEMS_PER_PAGE) {
        loadPage(++currentPage);
    }
}
void MainWindow::onLastClicked() {
    int totalPages = (mCountLines % ITEMS_PER_PAGE == 0
            ? mCountLines/ITEMS_PER_PAGE : mCountLines/ITEMS_PER_PAGE+1);
    int lastPage = totalPages-1;
    loadPage(currentPage = lastPage);
}

void MainWindow::onResultsTableClicked(const QModelIndex &i) {
    if (!i.isValid()) {
        return;
    }
    ui->ignore_button->setEnabled(true);
}

#-------------------------------------------------
#
# Project created by QtCreator 2015-04-25T21:11:24
#
#-------------------------------------------------

QT += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG -= console

TARGET = earch2015-injector
TEMPLATE = app

SOURCES += \
    src/entities.cpp \
    src/fdl.cpp \
    src/injector.cpp \
    src/main.cpp \
    src/mainwindow.cpp \
    src/models.cpp \
    src/optionsdialog.cpp \
    src/querydialog.cpp \
    src/validator.cpp \
    src/utils.cpp

HEADERS += \
    include/entities.h \
    include/fdl.h \
    include/injector.h \
    include/mainwindow.h \
    include/models.h \
    include/optionsdialog.h \
    include/querydialog.h \
    include/validator.h \
    include/utils.h

INCLUDEPATH += include

FORMS += \
    mainwindow.ui \
    optionsdialog.ui \
    querydialog.ui

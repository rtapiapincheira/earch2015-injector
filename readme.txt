Desarrollado en Windows7 64bits
- PostgreSQL version 3.9.8
- QtCreator version 5.3+
- Sistema operativo Windows 7 x86_64

---------------------- Instrucciones de compilación / ejecución ----------------------

1) Instalar y configurar PostgreSQL
   
    i) Descargar aplicacion desde los repositorios de Ubuntu

        $ sudo apt-get upgrade
        $ sudo apt-get update
        $ sudo apt-get install postgresql postgresql-contrib

    ii) Cambiar contrasena al usuario posgtres que se crea por defecto
   
        $ sudo -u postgres psql postgres
        postgresql=# \password postgres
        Enter new password: adminpawd1
        Enter it again: adminpawd1
        Ctrl+D

    iii) Crear base de datos

        $ sudo -u postgres createdb injector_db
        
    iv) Probar conexion (verificar version y puerto en el que corre PostgreSQL)

        $ sudo -u postgres psql -c "select version();"
        $ sudo -u postgres psql -c "select version();"

2) Instalar Git para descargar el repositorio

    $ sudo apt-get install git
    $ git --version

3) Instalar Qt, QMake, gcc y libqt5-sql-psql (database driver)
    $ sudo apt-get upgrade
    $ sudo apt-get update

    $ sudo apt-get install build-essential
    $ g++ --version

    $ sudo apt-get install qt5-default qt5-qmake
    $ qmake -v

    $ sudo apt-get install libqt5sql5-psql

3) Descargar codigo fuente desde Bitbucket (repositorio con permiso de lectura publico)

    $ mkdir ~/earch2015-injector
    $ cd ~/earch2015-injector
    $ git clone https://rtapiapincheira@bitbucket.org/rtapiapincheira/earch2015-injector.git .

4) Compilar/ejecutar aplicacion

    $ qmake earch2015-injector.pro
    $ make
    $ ./earch2015-injector

5) Seguir manual de uso para detalles de usabilidad
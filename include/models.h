#ifndef MODELS_H
#define MODELS_H

#include <QDebug>
#include <QtCore>
#include <QAbstractTableModel>

#include <QSqlQuery>

#include "validator.h"

class CrossedModel : public QAbstractTableModel {
    Q_OBJECT
public:
    explicit CrossedModel(QObject *parent = 0);

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;

    void setList(const QList<CrossedEntity> &list);

private:
    QList<CrossedEntity> objects;
};

//--------------------------------------------------------------------------------------------------

class PendingListener {
public:
    virtual void onChanged() = 0;
};

class PendingModel : public QAbstractTableModel {
    Q_OBJECT
public:
    explicit PendingModel(QObject *parent = 0);

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;

    QList<PendingEntity> getList() const;
    void setList(const QList<PendingEntity> &list);
    void setValidator(Validator validator);
    void setListener(PendingListener *listener);

    void ignoreEntity(int i);

private:
    Validator mValidator;
    PendingListener *mListener;
    QList<PendingEntity> objects;
};

#endif // MODELS_H

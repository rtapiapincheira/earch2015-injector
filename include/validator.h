#ifndef VALIDATOR_H
#define VALIDATOR_H

#include <QDateTime>
#include <QDebug>
#include <QtCore>
#include <QFile>

#include "fdl.h"
#include "entities.h"

class Validator {
private:
    FileDescriptorLanguage *mFdl;

public:
    Validator();
    Validator(FileDescriptorLanguage *fdl);
    ~Validator();

    bool validLine(const QStringList &parts, QString &reason, CrossedEntity *c);

    static bool validFdl(FileDescriptorLanguage *fdl);
    static bool validFdl(FileDescriptorLanguage *fdl, QString &reason);

    QString getDv(int num) const;

    bool validRut(const QString &, QString &) const;
    bool validEnterpriseCode(const QString &, QString &) const;
    bool validProductCode(const QString &, QString &) const;
    bool validDate(const QString &, QString &) const;
    bool validMoneyAmount(const QString &, QString &) const;

    bool validName(const QString &, QString &) const;
    bool validUnsignedInteger(const QString &, QString &) const;
    bool validAddress(const QString &, QString &) const;

    bool validSiiCode(const QString &, QString &) const;
    bool validGloss(const QString &, QString &) const;

    bool validPendingEntity(const PendingEntity &p) const;
};

#endif // VALIDATOR_H

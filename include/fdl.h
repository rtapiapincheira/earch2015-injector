#ifndef FDL_H
#define FDL_H

#include <QList>
#include <QRegExp>
#include <QtCore>

#include "entities.h"

#define MASTER_REGEXP "^(.*\\S.*)\\s*<\\s*(\\S+)\\s*:(\\S+)\\s*>$"

class FileDescriptorLanguage {
private:
    QFile *mFile;
    QStringList mFields;

public:
    QMap<QString,Field> mByTitle;
    QMap<QString,Field> mByColumn;
    QVector<Field> mByIndex;

    void parseMapping(const QStringList &list);

    static QStringList VALIDATIONS;

    FileDescriptorLanguage(QFile *file);

    /** Retorna false si no pudo procesar el archivo, o el archivo estaba malo. Retorna true si todo
      * estaba en orden. */
    bool parse();

    QStringList getFields() const;
    int getFieldsSize() const;

    PendingEntity getPendingEntity(const QStringList &parts);
    QStringList getAsLine(const PendingEntity &p);
};

#endif // FDL_H

#ifndef OPTIONSDIALOG_H
#define OPTIONSDIALOG_H

#include <QCloseEvent>
#include <QDebug>
#include <QDialog>
#include <QtCore>
#include <QMessageBox>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>

#include "ui_optionsdialog.h"

class OptionsDialog : public QDialog {
    Q_OBJECT
private:
    Ui::OptionsDialog *ui;
    QString m_host;
    QString m_username;
    QString m_password;
    quint16 m_port;
    QString m_database;

public:
    explicit OptionsDialog(QWidget *parent = 0);
    ~OptionsDialog();

    int exec();
    bool validParameters();
    QSqlDatabase getDb();

public slots:
    void okClicked();
    void testConnectionClicked();
    void recreateSchemaClicked();

    void onParameterChanged(const QString &newText);
};

#endif // MAINWINDOW_H

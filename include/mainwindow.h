#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QDateTime>
#include <QDebug>
#include <QDialog>
#include <QFile>
#include <QFileDialog>
#include <QMainWindow>
#include <QMessageBox>

#include "optionsdialog.h"
#include "querydialog.h"

#include "ui_mainwindow.h"

#include "validator.h"
#include "injector.h"

#define ITEMS_PER_PAGE          12
#define INSERTS_PER_TRANSACTION 150

#define GOOD_FILE "good.txt"
#define PENDING_PREFFIX "pending"
#define IGNORED_FILE "ignored.txt"

class MainWindow : public QMainWindow, public PendingListener, public ProgressHandler {
    Q_OBJECT
private:
    Ui::MainWindow *ui;

    FileDescriptorLanguage *mFdl;

    Injector injector;

    int currentPage;
    int mCountLines;
    int mGoodLines;

    qulonglong mPreTotalLines;

    PendingModel *pending_model;

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void updateButtons();
    void loadParameters();
    void saveParameters();

    QStringList getSlice(int page);

    void loadPage(int page);
    void savePage(int page, const QStringList &lines);

    virtual void onChanged();

    virtual void onUpdate(double percent);
    virtual void onTitle(const QString &title);

public slots:
    void onOpenFileClicked();
    void onOpenFdlClicked();
    void onExitClicked();
    void onQueryClicked();
    void onOptionsClicked();
    void onCleanClicked();

    void onProcessFileClicked();
    void onInsertBatchClicked();
    void onIgnoreClicked();
    void onSaveCorrectionsClicked();

    void onFirstClicked();
    void onPreviousClicked();
    void onNextClicked();
    void onLastClicked();

    void onResultsTableClicked(const QModelIndex &i);
};

#endif // MAINWINDOW_H

#ifndef QUERYDIALOG_H
#define QUERYDIALOG_H

#include <QCloseEvent>
#include <QDebug>
#include <QDialog>
#include <QtCore>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QMessageBox>

#include "ui_querydialog.h"

#include "models.h"

class QueryDialog : public QDialog {
    Q_OBJECT
private:
    Ui::QueryDialog *ui;

    QString m_host;
    QString m_username;
    QString m_password;
    quint16 m_port;
    QString m_database;

    CrossedModel *crossed_model;

    int m_page;

    QSqlDatabase getDb();

    static const int RESULTS_PER_PAGE;

    int getTotalCount();
    void update(int currentPage);

public:
    explicit QueryDialog(QWidget *parent = 0);
    ~QueryDialog();

    int exec();

public slots:
    void onFirstClicked();
    void onPreviousClicked();
    void onNextClicked();
    void onLastClicked();
};

#endif // QUERYDIALOG_H

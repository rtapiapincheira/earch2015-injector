#ifndef INJECTOR_H
#define INJECTOR_H

#include <QDateTime>
#include <QDebug>
#include <QtCore>
#include <QFile>
#include <QSqlError>

#include "validator.h"
#include "fdl.h"

#define PAGE_FACTOR 100

class ProgressHandler {
public:
    virtual void onUpdate(double percent) = 0;
    virtual void onTitle(const QString &title) = 0;
};

class Injector {
private:
    Validator validator;
    ProgressHandler *progressHandler;
public:
    explicit Injector(ProgressHandler *ph);
    ~Injector();

    void setup(FileDescriptorLanguage *fdl);
    QString processFile(const QString &filename, const QString &goodFile,
            const QString &pendingPreffix, int itemsPerPage, int &numTotalLines, int &numGoodLines,
            qulonglong &preTotalLines);

    QList<PendingEntity> getEntities(const QStringList &l, FileDescriptorLanguage *fdl);

    QString injectFromFiles(const QString &good, int numPendings, const QString &preffix,
            int insertsPerTransaction, qulonglong estimatedTotal);

    QString countLines(int numFiles, const QString &pendingPreffix,
            int &ignoredCount, int &correctedCount, int &invalidCount);

private:
    QString executeTransaction(QSqlDatabase &db, const QStringList &queries);

    bool writeToFile(const QString &filename, const QStringList &list);

    QSqlDatabase getDb();
};

#endif // INJECTOR_H

#ifndef ENTITIES_H
#define ENTITIES_H

#include <QDebug>
#include <QtCore>
#include <QAbstractTableModel>

#include <QSqlQuery>

typedef struct {
    QString title;
    QString column;
    QString type;
} Field;

class CrossedEntity {
public:

    static QStringList COLUMNS;

    quint32 accountNumber;
    quint32 rutA;
    QString enterpriseCode;
    QString productCode;
    QDate datePost;
    quint32 amountPost;
    QDate dateContract;

    quint32 rutFather;
    QString fFirstName;
    QString fLastName;
    quint32 rutMother;
    QString mFirstName;
    QString mLastName;
    QDate marriageDate;
    quint16 childrenNumber;
    quint16 childrenNumberUnderAge;
    QString address;

    quint32 rutB;
    QString firstName;
    QString lastName;
    quint32 actives;
    QString code;
    QString gloss;

    CrossedEntity();
    CrossedEntity(QSqlQuery &q);

    void assignByField(const Field &f, const QString &s);

    static QString escape(const QString &s);
    QString getInsertQuery() const;
};

class PendingEntity {
public:
    QString accountNumber;
    QString rutA;
    QString enterpriseCode;
    QString productCode;
    QString datePost;
    QString amountPost;
    QString dateContract;

    QString rutFather;
    QString fFirstName;
    QString fLastName;
    QString rutMother;
    QString mFirstName;
    QString mLastName;
    QString marriageDate;
    QString childrenNumber;
    QString childrenNumberUnderAge;
    QString address;

    QString rutB;
    QString firstName;
    QString lastName;
    QString actives;
    QString code;
    QString gloss;

    bool isIgnored() const;

    PendingEntity();
    PendingEntity(const QVector<QString> &q);

    CrossedEntity toCrossedEntity();
    QStringList getParts() const;
};

#endif // ENTITIES_H

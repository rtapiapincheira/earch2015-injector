----- Drop everything ------------------------------------------------------------------------------
BEGIN;
DROP TABLE crossing;
COMMIT;
----------------------------------------------------------------------------------------------------
BEGIN;
----- Table Family ---------------------------------------------------------------------------------
CREATE TABLE crossing(rut_father integer NOT NULL,f_first_name text NOT NULL,f_last_name text NOT NULL,rut_mother integer NOT NULL,m_first_name text NOT NULL,m_last_name text NOT NULL,marriage_date date NOT NULL,children_number smallint NOT NULL,children_number_under_age smallint NOT NULL,address text NOT NULL,account_number integer NOT NULL,rut_a integer NOT NULL,enterprise_code text NOT NULL,product_code text NOT NULL,date_post date NOT NULL,amount_post integer NOT NULL,date_contract date NOT NULL,rut_b integer NOT NULL,first_name text NOT NULL,last_name text NOT NULL,actives integer NOT NULL,code text NOT NULL,gloss text NOT NULL)WITH(OIDS=FALSE);

CREATE INDEX family_rut_father_idx ON crossing USING btree(rut_father);
CREATE INDEX family_rut_mother_idx ON crossing USING btree(rut_mother);
CREATE INDEX savings_account_rut_idx ON crossing USING btree(rut_a);
CREATE INDEX sii_origin_rut_idx ON crossing USING btree(rut_b);
     
COMMIT;